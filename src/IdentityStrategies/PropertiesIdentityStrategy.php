<?php

namespace Ksure\Collections\IdentityStrategies;

class PropertiesIdentityStrategy implements IdentityStrategy
{
    /**
     * Список свойств сущности, по которым будет проводиться идентификация
     * @var string[]
     */
    protected $identityProperties = [];

    /**
     * EntityProperties constructor.
     * @param array $identityProperties
     */
    public function __construct(array $identityProperties)
    {
        $this->identityProperties = $identityProperties;
    }

    /**
     * @param $element
     * @return string
     */
    public function getIdentity($element)
    {
        $identity = [];

        foreach ($this->identityProperties as $property) {
            $identity[] = $element->{$property};
        }

        return join('.', $identity);
    }

}