<?php

namespace Ksure\Collections\IdentityStrategies;

interface IdentityStrategy
{
    /**
     * @param $element
     * @return string
     */
    public function getIdentity($element);
}