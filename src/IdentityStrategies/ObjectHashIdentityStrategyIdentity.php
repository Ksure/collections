<?php

namespace Ksure\Collections\IdentityStrategies;

class ObjectHashIdentityStrategy implements IdentityStrategy
{
    /**
     * @param $element
     * @return string
     */
    public function getIdentity($element)
    {
        return spl_object_hash($element);
    }

}