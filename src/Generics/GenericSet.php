<?php

namespace Ksure\Collections\Generics;

use Ksure\Collections\IdentityStrategies\ObjectHashIdentityStrategy;
use Ksure\Collections\IdentityStrategies\PropertiesIdentityStrategy;
use Ksure\Collections\IdentityStrategies\IdentityStrategy;
use Exception;
use Ksure\Collections\Set;

class GenericSet extends Set
{
    /**
     * Тип (класс) элементов
     * @var string
     */
    private $type;

    /**
     * GenericEntitySet constructor.
     * @param IdentityStrategy $type
     * @param array $identityProperties
     */
    public function __construct($type, array $identityProperties = null)
    {
        $this->type = $type;

        $identityStrategy = $identityProperties ? new PropertiesIdentityStrategy($identityProperties) : new ObjectHashIdentityStrategy();
        
        parent::__construct($identityStrategy);
    }

    /**
     * Добавить элемент
     * @param $element
     * @throws Exception
     */
    public function add($element)
    {
        if (!($element instanceof $this->type)) {
            throw new Exception('Undefined type');
        }

        parent::add($element);
    }

    /**
     * Удалить элемент
     * @param $element
     * @throws Exception
     */
    public function remove($element)
    {
        if (!($element instanceof $this->type)) {
            throw new Exception('Undefined type');
        }

        parent::remove($element);
    }

    /**
     * Проверить наличие элемента в наборе
     * @param $element
     * @return boolean
     * @throws Exception
     */
    public function has($element)
    {
        if (!($element instanceof $this->type)) {
            throw new Exception('Undefined type');
        }

        parent::has($element);
    }
}