<?php

namespace Ksure\Collections;

use IteratorAggregate;

interface Collection extends IteratorAggregate
{
    
}