<?php

namespace Ksure\Collections;

use ArrayIterator;

/**
 * Class Set
 * @package Collections
 */
abstract class Set implements Collection
{
    /**
     * Массив элементов
     * @var array
     */
    protected $elements = [];

    /**
     * Калькулятор (стратегия) расчёта уникального идентификатора елементов
     * @var IdentityStrategies\IdentityStrategy
     */
    protected $identityStrategy;

    /**
     * Set constructor.
     * @param IdentityStrategies\IdentityStrategy $identityStrategy
     */
    public function __construct(IdentityStrategies\IdentityStrategy $identityStrategy)
    {
        $this->identityStrategy = $identityStrategy;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->toArray());
    }

    /**
     * Добавить элемент
     * @param $element
     */
    public function add($element)
    {
        $identity = $this->getIdentity($element);

        if (!isset($this->elements[$identity])) {
            $this->elements[$identity] = $element;
        }
    }

    /**
     * Удалить элемент
     * @param $element
     */
    public function remove($element)
    {
        $identity = $this->getIdentity($element);

        if (isset($this->elements[$identity])) {
            unset($this->elements[$identity]);
        }
    }

    /**
     * Проверить наличие элемента в наборе
     * @param $element
     * @return boolean
     */
    public function has($element)
    {
        $identity = $this->getIdentity($element);

        return isset($this->elements[$identity]);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->elements;
    }

    /**
     * Возвращает копию коллекции
     * @return static
     */
    public function copy()
    {
        $copy = new static($this->identityStrategy);

        foreach ($this as $element) {
            $copy->add(clone $element);
        }

        return $copy;
    }

    /**
     * Сброс (пустое множество)
     */
    public function reset()
    {
        $this->elements = [];
    }

    /**
     * Возвращает уникальный идентификатор элемента
     * @param $element
     * @return string
     */
    protected function getIdentity($element)
    {
        return $this->identityStrategy->getIdentity($element);
    }
}